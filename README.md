Project - Computer Networking
=======================================

Joao Rocha - no. 2100 09 82
------------------------

**ProjectoRedes** : a Client-Server application developed Java that uses a multi-threading logic to allow for multiple client connections to the same server through the use of TCP Sockets.

Project Requirements
--------------------

**The completed project should be able to:**

- Implement an IP Address Blacklist to prevent blacklisted IP Addresses from connecting to the server.
- List all Online Users.
- Send messages through UDP Datagrams to a single user.
- Send messages through UDP Datagrams to all online users.


Changelog
==========

Version 1.0
-----------

- Working *IP Blacklist*
- Working *List of all Online Users*


