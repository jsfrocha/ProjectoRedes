package pt.ulht.redes.cliente;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class EchoClient2 {	
	public static void main(String args[]) throws Exception {
		if (args.length !=1){
			System.err.println ("usage: java EchoClient2 <host>");
			System.exit(1);
		}				
		String host = args[0];
		int port = 6500;
		String cmd, line;
		Socket socket = new Socket(host,port);
		BufferedReader input = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
		PrintStream output = new PrintStream(socket.getOutputStream(),true);

			while( true ) {//Input cycle 

				Scanner scan = new Scanner (System.in);
				if (input.readLine().equals("***BLACKLISTED***")) {
					System.out.println("IP is Blacklisted");
					break;
				}
				System.out.println(" ");
				System.out.println("CLIENT MENU");
				System.out.println(" ");
				System.out.println("1 - List on-line users");
				System.out.println("2 - Send message to a single user");
				System.out.println("3 - Send message to all on-line users");
				System.out.println("4 - List Blacklisted Users");
				System.out.println("9 - Exit");
				System.out.println(" ");
				System.out.print(host+":"+port+"#>");	//Command prompt 			
				cmd = scan.nextLine();	//Scanning command to send to the server	
				output.println(cmd); 	//Sending command to the server
				
				if ( cmd.equalsIgnoreCase("9")){
					System.out.println("Exiting..");
					break;
				} 
				
				try {
				while (!(line = input.readLine()).equals("***CLOSE***")) {	//Input Cycle							
				System.out.println (line);	//Prints server answer		
				}
				} catch (Exception e) {
					System.err.println("Client Side Error!");
					System.out.println(e);
					break;
				}
				
			}//End of Cycle
		
		System.out.println("Connection Terminated");
		input.close();	//Closes inputStream
		output.close();	//Closes outputStream
		socket.close();	//Closes Socket	
	}
}