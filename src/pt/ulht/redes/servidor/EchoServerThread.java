package pt.ulht.redes.servidor;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;
import java.io.*;

public class EchoServerThread {
	
	public static ArrayList<String> f = new ArrayList<String>();
	public static ArrayList<String> userList = new ArrayList<String>();
	public static int index;
	public static String date;
	public static void main(String args[]) throws Exception {//inicio main		
		
		
		ServerSocket server = new ServerSocket(6500);	//Create socket on port 6500
		System.out.println ("Server started on port 6500");		
		while (true){	//Waiting for clients
			System.out.println("Server waiting for client connections..");
			Socket socket = null;
			BufferedReader reader = new BufferedReader(new FileReader("C:\\UNIV\\Redes\\workspace\\Ex_4.3_ListOnline_Blacklist\\lists\\blacklist.txt"));
			String line = null;
			socket = server.accept();
			
			// Blacklist verification
			while ((line = reader.readLine()) != null) {
				if (line.equals(socket.getInetAddress().toString())) {
					System.out.println("IP Blacklisted: " + socket.getInetAddress().toString());
					System.out.println("Closing connection to " + socket.getInetAddress().toString());
					PrintStream checkBlack = new PrintStream(socket.getOutputStream(),true);
					checkBlack.println("***BLACKLISTED***");
					reader.close();
					checkBlack.close();
					socket.close();
					break; 
				}
			}//End of Blacklist Verification
			
			//Sending feedback in case of approved client
			try { 
				socket.getOutputStream().write("***NBLACKLISTED***\n".getBytes());
			} catch (SocketException e) {
				System.out.println("Client Blacklisted, will not send approval.");
			}
			userList.add(socket.getInetAddress().toString()); //Add connected user's IP to USERLIST
			System.out.println("New connection..");
			System.out.println("Size of UserList: " + userList.size());
			Thread t = new Thread(new EchoClientThread(socket)); 
			
			t.start(); //Starting Client Thread
			
			
		}//End of Waiting for Clients 		
	}//End of Main	

	public static class EchoClientThread implements Runnable{
		private Socket s;
		public EchoClientThread(Socket socket) {
			this.s = socket;
		}
		public void run() {	
			String threadName = Thread.currentThread().getName();	//Thread Name
			String stringClient = s.getInetAddress().toString();	//Client IP
			System.out.println("Connected to " + stringClient);	
			
			try{				
				BufferedReader input = new BufferedReader(
						new InputStreamReader(s.getInputStream()));			
				PrintStream output = new PrintStream(
						s.getOutputStream(),true);
				String line;
				while ((line = input.readLine()) !=null) {	//Input Cycle	
					
					System.out.println (stringClient+": "+threadName+": "+line);	//Print command from client
					
					if (line.equalsIgnoreCase("9")){ //Exit
						break;
					}

					else if (line.equalsIgnoreCase("1")){	//Send List of Online Users
						System.out.println("Option 1: Sending list of online users to " + stringClient);
						output.println(" ");
						output.println("List of Online Users:");
						output.println(" ");
						for(int i=0;i<userList.size();i++){
							output.println(userList.get(i));
						}
					}
					
					else if (line.equalsIgnoreCase("2")) {	//Send message to a single user
						System.out.println("Nothing here yet..");
					}
					
					else if (line.equalsIgnoreCase("3")) {	//Send message to all the online users
						System.out.println("Nothing here yet..");
					}
					
					else if (line.equalsIgnoreCase("4")){	//Send User Blacklist
						System.out.println("Option 4: Sending user blacklist to " + stringClient);
						BufferedReader reader = new BufferedReader(new FileReader("C:\\UNIV\\Redes\\workspace\\Ex_4.3_ListOnline_Blacklist\\lists\\blacklist.txt"));
						String lineRead = null;
						output.println(" ");
						output.println("User Blacklist:");
						output.println(" ");
						while ((lineRead = reader.readLine()) != null) {
							output.println(lineRead);
						}
						reader.close();
					}
					
					else{
						output.println("Unknown command.");
					}	
					
					output.println("***CLOSE***");	//Closes client's input cycle
					output.println("***NBLACKLISTED***");	//Sending feedback in case of approved client						
				}//Input Cycle End
				
				output.println("See you later!"); 
				input.close();	//Closes inputStream 
				output.close();	//Closes outputStream
				s.close();	//Closes Socket							
			}
			catch (SocketException e){
				System.out.println("Client Blacklisted - Closing Client Thread..");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			userList.remove(s.getInetAddress().toString());
			System.out.println("Client "+ stringClient+" was disconnected!");					
		}//End of run()  
	}//End of EchoClientThread
}//End of EchoServerThread 


